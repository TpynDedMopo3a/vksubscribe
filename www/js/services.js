/**
 * Created by tpyndedamopo3a on 06.06.15.
 */
var app = angular.module('starter.services', ['ngResource']);

app.factory('$localstorage', ['$window', function ($window) {

    try {
        var set = function (key, value) {
            $window.localStorage[key] = value;
        };
        var get = function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        };
        var setObject = function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        };
        var getObject = function (key, defaultValue) {
            return JSON.parse($window.localStorage[key] || defaultValue || '{}');
        };
        // post
        var setPost = function (post) {
            setObject('post_sId=' + post.subscribeId + '&pId=' + post.id, post);
        };
        var getPost = function (subscribeId, postId) {
            return getObject('post_sId=' + subscribeId + '&pId=' + postId, 'null');
        };
        // subscribe
        var setSubscribe = function (subscribe) {
            setObject('subscribe_sId=' + subscribe.id, subscribe);
        };
        var getSubscribe = function (subscribeId) {
            return getObject('subscribe_sId=' + subscribeId, 'null');
        };
    }
    catch (err) {
        alert('LocalStorage error: ' + err);
    }
    return {
        set: set,
        get: get,
        setObject: setObject,
        getObject: getObject,
        setPost: setPost,
        getPost: getPost,
        setSubscribe: setSubscribe,
        getSubscribe: getSubscribe
    }
}]);


app.factory('VK_api', function ($q, $resource, $http) {
    //var proxyServer = 'http://109.87.140.64:12341';
    var proxyServer = 'http://atom-m.net:12341';
    var wall = {};
    var groups = {};

    var post = function (data) {
        var req = {
            'method': 'POST',
            'url': proxyServer + '/vk_api',
            'responseType': 'json',
            'headers': {
                'Content-Type': 'application/json; charset=utf-8'
            },
            'data': JSON.stringify(data)
        };
        return $http(req);
    };

    wall.get = function (owner_id, offset, count) {
        /*owner_id	идентификатор пользователя или сообщества, со стены которого необходимо получить записи (по умолчанию — текущий пользователь).

         Обратите внимание, идентификатор сообщества в параметре owner_id необходимо указывать со знаком "-" — например, owner_id=-1 соответствует идентификатору сообщества ВКонтакте API (club1)


         целое число
         domain	короткий адрес пользователя или сообщества.
         строка
         offset	смещение, необходимое для выборки определенного подмножества записей.
         положительное число
         count	количество записей, которое необходимо получить (но не более 100).
         положительное число
         filter	определяет, какие типы записей на стене необходимо получить. Возможны следующие значения параметра:

         suggests — предложенные записи на стене сообщества (доступно только при вызове с передачей access_token);
         postponed — отложенные записи (доступно только при вызове с передачей access_token);
         owner — записи на стене от ее владельца;
         others — записи на стене не от ее владельца;
         all — все записи на стене (owner + others).



         Если параметр не задан, то считается, что он равен all.
         строка
         extended	1 — будут возвращены три массива wall, profiles и groups. По умолчанию дополнительные поля не возвращаются.
         флаг, может принимать значения 1 или 0*/
        var deferred = $q.defer();
        //console.log("wall.get: [" + owner_id + "]");
        //var q = {};
        //q.owner_id = '-' + (owner_id || 1);
        //q.offset = offset || 0;
        //q.count = count || 60;
        //$http.get(proxyServer + '/vk_wall_get?owner_id=' + owner_id + '&offset=' + offset + '&count=' + count).success(function (data) {
        //    console.log("new message: ", data);
        //    deferred.resolve(data);
        //}).error(function (error) {
        //    deferred.reject('Undefined network error');
        //});

        console.log("wall.get: [" + owner_id + "]");
        var q = {};
        q.method = 'wall.get';
        q.owner_id = '-' + (owner_id || 1);
        q.offset = offset || 0;
        q.count = count || 25;
        post(q).success(function (data) {
            console.log("new message: " + JSON.stringify(data));
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject('Undefined network error');
        });
        return deferred.promise;
    };

    groups.getById = function (group_id) {
        /*group_ids	идентификаторы или короткие имена сообществ.
         список строк, разделенных через запятую
         group_id	идентификатор или короткое имя сообщества.
         строка
         fields	список дополнительных полей, которые необходимо вернуть. Возможные значения: city, country, place, description, wiki_page, members_count, counters, start_date, finish_date, can_post, can_see_all_posts, activity, status, contacts, links, fixed_post, verified, site,ban_info.
         список строк, разделенных через запятую*/
        var deferred = $q.defer();
        console.log("groups.getById: [" + group_id + "]");
        var q = {};
        q.method = 'groups.getById';
        q.group_id = '' + (group_id || 0);
        q.fields = 'description, status';
        post(q).success(function (data) {
            console.log("new message: ", data);
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject('Undefined network error');
        });
        return deferred.promise;
    };

    groups.search = function (text) {
        /*q	текст поискового запроса.
         строка, обязательный параметр
         type	тип сообщества. Возможные значения: group, page, event.
         строка
         country_id	идентификатор страны.
         положительное число
         city_id	идентификатор города. При передаче этого параметра поле country_id игнорируется.
         положительное число
         future	при передаче значения 1 будут выведены предстоящие события. Учитывается только при передаче в качестве type значения event.
         флаг, может принимать значения 1 или 0
         sort
         0 — сортировать по умолчанию (аналогично результатам поиска в полной версии сайта);
         1 — сортировать по скорости роста;
         2 — сортировать по отношению дневной посещаемости к количеству пользователей;
         3 — сортировать по отношению количества лайков к количеству пользователей;
         4 — сортировать по отношению количества комментариев к количеству пользователей;
         5 — сортировать по отношению количества записей в обсуждениях к количеству пользователей.

         целое число
         offset	смещение, необходимое для выборки определённого подмножества результатов поиска. По умолчанию — 0.
         положительное число
         count	количество результатов поиска, которое необходимо вернуть. */
        var deferred = $q.defer();
        console.log("groups.search: [" + text + "]");
        var q = {};
        q.method = 'groups.search';
        q.q = '' + text;
        q.offset = 0;
        q.count = 15;
        post(q).success(function (data) {
            console.log("new message: ", data);
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject('Undefined network error');
        });
        return deferred.promise;
    };

    //
    var methods = {
        wallGet: wall.get,
        groupsGetById: groups.getById,
        groupsSearch: groups.search
    };

    return methods;
});

//app.factory('ws', function ($q, $websocket) {
//    // Open a WebSocket connection
//    //var dataStream = $websocket('ws://atom-m.net:12341');
//    var dataStream = $websocket('ws://127.0.0.1:12341');
//
//    var deferred = null;
//    var collection = [];
//
//    dataStream.onMessage(function (message) {
//        var msg = JSON.parse(message.data);
//        console.log("new message: ", msg);
//        deferred.resolve(msg);
//    });
//
//    var methods = {
//        getPosts: function (id, offset, count) {
//            deferred = $q.defer();
//            console.log("send message");
//            dataStream.send(JSON.stringify({
//                cmd: 'getPosts',
//                id: id,
//                offset: offset,
//                count: count
//            }));
//            return deferred.promise;
//        }
//    };
//
//    return methods;
//    //http://vk.com/wall-34976879?own=1
//});

app.factory('Subscribe', function ($q, $localstorage, VK_api) {
    var subscribeList = [];
    console.log($localstorage.getObject('subscribeList', 'null'));
    if ($localstorage.getObject('subscribeList', 'null') === null) {
        $localstorage.setObject('subscribeList', subscribeList);
        //
        var arr = [
            'g00d_life_inc',
            'major_jornal',
            'recollect',
            'menoutlook',
            'luxury___men',
            'mens.style.magazine',
            'carpeomnia',
            'ibtty',
            'oceanfuture'
        ];
        for (var ind in arr) {
            var uri = arr[ind];
            searchSubscribeByName(uri).then(function (data) {
                subscribeList.push(data[0]);
                $localstorage.setObject('subscribeList', subscribeList);
            });
        }
    }
    else {
        subscribeList = $localstorage.getObject('subscribeList', '[]');
    }

    function searchSubscribeByName(subscribeId) {
        var deferred = $q.defer();
        VK_api.groupsGetById(subscribeId).then(function (data) {
            deferred.resolve(parseGroups(data));
        });
        return deferred.promise;
    }


    var parseGroups = function (groups) {
        var result = [];
        groups.forEach(function (group, i, arr) {
            var parsed = parseGroup(group);
            result.push(parsed);
        });
        return result;
    };

    var parseGroup = function (group) {
        var result = {};
        result.id = group.id;
        result.name = group.name;
        result.status = group.status;
        result.description = group.description;
        result.photo_50 = group.photo_50;
        return result;
    };

    return {
        searchSubscribe: function (subscribeName) {
            var deferred = $q.defer();
            VK_api.groupsSearch(subscribeName).then(function (data) {
                deferred.resolve(parseGroups(data));
            });
            return deferred.promise;
        },
        searchSubscribeById: searchSubscribeByName,
        addSubscribe: function (subscribe) {
            subscribeList.push(subscribe);
            $localstorage.setObject('subscribeList', subscribeList);
        },
        getSubscribes: function () {
            return subscribeList;
        },
        getSubscribe: function (subscribeId) {
            var subscribe = null;
            console.log('getSubscribe(' + subscribeId + ')');
            subscribeList.forEach(function (group, i, arr) {
                if (group.id == subscribeId) {
                    subscribe = group;
                }
            });
            console.log('getSubscribe(' + subscribeId + ') return:' + JSON.stringify(subscribe));
            return subscribe;
        }
    };
});

app.factory('ImgToBase64', function () {

    /**
     * Convert an image to a base64 url
     * @param {String}  url
     * @param {Function}  callback
     * @param {String} [outputFormat=image/png]
     */
    var convertImageToBase64 = function (url, callback, outputFormat) {
        //callback(url);
        //return;
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            var canvas = document.createElement('canvas');
            var dataUrl;
            canvas.height = this.height;
            canvas.width = this.width;
            var context = canvas.getContext('2d');
            context.drawImage(this, 0, 0);
            dataUrl = canvas.toDataURL(outputFormat);
            callback(dataUrl);
            canvas = null;
        };
        img.src = url;
    };

    return {
        convert: convertImageToBase64
    };
});


app.factory('PostsOLD', function ($q, $localstorage, Subscribe, VK_api, ImgToBase64) {
    //$localstorage.setObject('subscribeList', subscribeList);
    // {id:0, posts:[]}
    var posts = {};

    //var favorites = {};

    var parsePosts = function (publicId, response) {
        var result = {
            id: publicId,
            count: 0,
            posts: []
        };

        response.items.forEach(function (post, i, arr) {
            var p = parsePost(publicId, post);
            //console.log("POST #" + i + ":  ", p);
            result.posts.push(p);
            result.count++;

        });

        //console.log("POSTS: ", result.count);

        return result;
    };
    var parsePost = function (publicId, post) {
        var result = {
            id: post.id,
            date: post.date,
            text: post.text.replace(new RegExp("\n", "gm"), "<br>"),
            url: 'http://vk.com/wall-' + publicId + '_' + post.id,
            photosCount: 0,
            photos: [],
            likes: post.likes.count,
            reposts: post.reposts.count
        };
        if (post.attachments) {
            post.attachments.forEach(function (attachment, i, arr) {
                if (attachment.type === 'photo') {
                    var url = null;
                    if (attachment.photo.photo_2560) {
                        url = attachment.photo.photo_2560;
                    }
                    else if (attachment.photo.photo_1280) {
                        url = attachment.photo.photo_1280;
                    }
                    else if (attachment.photo.photo_807) {
                        url = attachment.photo.photo_807;
                    }
                    else if (attachment.photo.photo_604) {
                        url = attachment.photo.photo_604;
                    }
                    else if (attachment.photo.photo_130) {
                        url = attachment.photo.photo_130;
                    }
                    else if (attachment.photo.photo_75) {
                        url = attachment.photo.photo_75;
                    }
                    if (url !== null) {
                        ImgToBase64.convert(url, new function () {
                            result.photos.push(url);
                            result.photosCount++;
                        });
                    }
                }
            });
        }
        return result;
    };

    var updatePosts = function (subscribeId) {
        var deferred = $q.defer();
        VK_api.wallGet(subscribeId).then(function (parsed) {
            posts['' + subscribeId] = parsed.posts;
            savePosts(subscribeId);
            deferred.resolve(parsed.posts);
        });
        //
        //VK_api.wallGet(subscribeId).then(function (data) {
        //    var parsed = parsePosts(subscribeId, data);
        //    posts['' + subscribeId] = parsed.posts;
        //    VK_api.wallGet(subscribeId, 100).then(function (data) {
        //        var parsed = parsePosts(subscribeId, data);
        //        posts['' + subscribeId] = posts['' + subscribeId].concat(parsed.posts);
        //        VK_api.wallGet(subscribeId, 200).then(function (data) {
        //            var parsed = parsePosts(subscribeId, data);
        //            posts['' + subscribeId] = posts['' + subscribeId].concat(parsed.posts);
        //            VK_api.wallGet(subscribeId, 300).then(function (data) {
        //                var parsed = parsePosts(subscribeId, data);
        //                posts['' + subscribeId] = posts['' + subscribeId].concat(parsed.posts);
        //                VK_api.wallGet(subscribeId, 400).then(function (data) {
        //                    var parsed = parsePosts(subscribeId, data);
        //                    posts['' + subscribeId] = posts['' + subscribeId].concat(parsed.posts);
        //                    savePosts(subscribeId);
        //                    deferred.resolve(parsed.posts);
        //                });
        //            });
        //        });
        //    });
        //});
        return deferred.promise;
    };

    var saveAllPosts = function () {
        for (var ind in posts) {
            savePosts(posts[ind].id);
        }
    };
    var savePosts = function (subscribeId) {
        $localstorage.setObject('posts_' + subscribeId, posts['' + subscribeId]);
    };
    var loadAllPosts = function () {
        var subscribes = Subscribe.getSubscribes();
        for (var ind in subscribes) {
            loadPosts(subscribes[ind].id);
        }
    };
    var loadPosts = function (subscribeId) {
        console.log("##########################################");
        console.log("loadPosts id: " + subscribeId);
        console.log("loadPosts list: " + posts['' + subscribeId]);
        console.log("loadPosts is_null: " + $localstorage.getObject('posts_' + subscribeId, 'null'));
        posts['' + subscribeId] = $localstorage.getObject('posts_' + subscribeId, 'null');
        posts['' + subscribeId] = (posts['' + subscribeId] === null) ? new Array() : posts['' + subscribeId];
    };


    //loadAllPosts();

    return {
        updatePosts: updatePosts,
        getBySubscribeIdWOUpdate: function (subscribeId) {
            if (!posts[subscribeId]) {
                loadPosts(subscribeId);
            }
            return posts[subscribeId];
        },
        getBySubscribeId: function (subscribeId) {
            var deferred = $q.defer();
            if (!posts[subscribeId]) {
                loadPosts(subscribeId);
            }
            if (posts[subscribeId].length === 0) {
                updatePosts.then(function (data) {
                    deferred.resolve(data);
                });
            }
            else {
                deferred.resolve(posts[subscribeId]);
            }
            return deferred.promise;
        },
        getPost: function (subscribeId, postId) {
            console.log('getPost(' + subscribeId + ')');
            for (var ind in posts[subscribeId]) {
                var post = posts[subscribeId][ind];
                if (post.id == postId) {
                    return post;
                }
            }
            return null;
        },
        getNextPost: function (subscribeId, postId) {
            console.log('getPost(' + subscribeId + ')');
            var nextPost = false;
            for (var ind in posts[subscribeId]) {
                var post = posts[subscribeId][ind];
                //if ((post.text) || (post.photosCount > 0)) {
                if (nextPost) {
                    return post;
                }
                if (post.id == postId) {
                    nextPost = true;
                }
                //}
            }
            return null;
        },
        getPreviousPost: function (subscribeId, postId) {
            console.log('getPost(' + subscribeId + ')');
            var prevPost = null;
            for (var ind in posts[subscribeId]) {
                var post = posts[subscribeId][ind];
                //if ((post.text) || (post.photosCount > 0)) {
                if (post.id == postId) {
                    return prevPost;
                }
                prevPost = post;
                //}
            }
            return null;
        },
        getPostByLocalId: function (subscribeId, localId) {
            return posts[subscribeId][localId];
        }
    };
});

//file:///android_asset/www/lib/ionic/js/ionic.bundle.js: Line 20306 : Error: QUOTA_EXCEEDED_ERR: DOM Exception 22
app.factory('Posts', function ($q, $localstorage, VK_api, ImgToBase64) {

    var SID = 0;
    var posts = {};

    var updatePosts = function (subscribeId) {
        var deferred = $q.defer();
        SID = subscribeId;
        VK_api.wallGet(subscribeId, 0, 100).then(function (resp) {
            console.log('RESPONSE 1: ' + resp);
            console.log('RESPONSE 2: ' + JSON.stringify(resp));
            console.log('RESPONSE 3: ' + resp.items.length);
            parsePosts(subscribeId, resp.items).then(function (parsed) {
                console.log('all posts parsed!');
                posts = parsed;
                $localstorage.setObject('posts_' + subscribeId, posts);
                deferred.resolve(posts);
            }, function () {
                console.log('all posts not parsed!');
                SID = 0;
                posts = {};
                deferred.reject(posts);
            });
        }, function () {
            console.log('VK_api error!');
            SID = 0;
            posts = {};
            deferred.reject(posts);
        });
        return deferred.promise;
    };

    var loadPosts = function (subscribeId) {
        var deferred = $q.defer();
        if (SID === subscribeId) {
            deferred.resolve(posts);
        }
        else {
            SID = subscribeId;
            var p = $localstorage.getObject('posts_' + subscribeId, 'null');
            if (p === null) {
                updatePosts(subscribeId).then(function (parsed) {
                    deferred.resolve(parsed);
                }, function (parsed) {
                    deferred.reject(parsed);
                });
            }
            else {
                posts = p;
                deferred.resolve(posts);
            }
        }
        return deferred.promise;
    };

    var parsePosts = function (subscribeId, items) {
        var deferred = $q.defer();
        console.log('parsePosts: items  ' + JSON.stringify(items));
        if ((items) && (items.length > 0)) {
            var result = null;
            var counter = items.length;
            for (var ind in items) {
                var post = items[ind];
                console.log('parsePosts: items[' + ind + '] -  ' + JSON.stringify(post));
                parsePost(subscribeId, post).then(function (parsed) {
                    console.log('parsePost OK: ' + JSON.stringify(parsed));
                    if ((parsed)
                        && (((parsed.text) && (parsed.text.length > 0))
                        || ((parsed.media) && (parsed.media.length > 0)))) {
                        if (result === null) {
                            result = {};
                        }
                        result[parsed.id] = parsed;
                        console.log('parsePost OK: add to list');
                    }

                    --counter;
                    if (counter <= 0) {
                        deferred.resolve(result);
                    }
                }, function () {
                    console.log('parsePost BAD');
                    --counter;
                    if (counter <= 0) {
                        if (result === null) {
                            deferred.reject();
                        }
                        else {
                            deferred.resolve(result);
                        }
                    }
                });
            }
        }
        else {
            deferred.reject();
        }
        return deferred.promise;
    };

    var parseDate = function (date) {
        var dt = new Date(Number(date + '000'));
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        return '' + ((day < 10) ? '0' + day : day) + '.' +
            ((month < 10) ? '0' + month : month) + '.' + year;
    };

    var parseTime = function (date) {
        var dt = new Date(Number(date + '000'));
        var hours = dt.getHours();
        var minutes = dt.getMinutes();
        return '' + ((hours < 10) ? '0' + hours : hours) + ':' +
            ((minutes < 10) ? '0' + minutes : minutes);
    };

    var parsePost = function (subscribeId, post) {
        console.log('parsePost: ' + JSON.stringify(post));
        var deferred = $q.defer();
        var result = {
            id: post.id,
            date: parseDate(post.date),
            time: parseTime(post.date),
            text: post.text.replace(new RegExp("\n", "gm"), "<br>"),
            media: null           //  {type: 'image', data: '...'}
        };
        if ((post.attachments) && (post.attachments.length > 0)) {
            result.media = [];
            var counter = post.attachments.length;
            for (var ind in post.attachments) {
                var attachment = post.attachments[ind];
                if (attachment.type === 'photo') {
                    var url = null;
                    //if (attachment.photo.photo_2560) {
                    //    url = attachment.photo.photo_2560;
                    //}
                    //else if (attachment.photo.photo_1280) {
                    //    url = attachment.photo.photo_1280;
                    //}
                    //else
                    if (attachment.photo.photo_807) {
                        url = attachment.photo.photo_807;
                    }
                    else
                    if (attachment.photo.photo_604) {
                        url = attachment.photo.photo_604;
                    }
                    else if (attachment.photo.photo_130) {
                        url = attachment.photo.photo_130;
                    }
                    else if (attachment.photo.photo_75) {
                        url = attachment.photo.photo_75;
                    }
                    if (url !== null) {
                        result.media.push({
                            type: 'photo',
                            data: url
                        });
                        --counter;
                        if (counter <= 0) {
                            deferred.resolve(result);
                        }
                        //ImgToBase64.convert(url, function (dataImg) {
                        //    result.media.push({
                        //        type: 'photo',
                        //        data: ''
                        //        //data: 'img_'+subscribeId+'_'+post.id
                        //    });
                        //    var img = dataImg;
                        //    var imgId = 'img_' + subscribeId + '_' + post.id;
                        //    var i = 0;
                        //    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
                        //    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
                        //    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
                        //    console.log('image source: ' + (img.length) + ' - ' + img);
                        //    var blockSize = 1024;
                        //    do {
                        //        var block = (img.length > blockSize) ? img.substr(i * blockSize, (i + 1) * blockSize) : img.substr(i * blockSize);
                        //        console.log('image block: ' + (block.length) + ' - ' + block);
                        //        if ((block) && (block.length > 0)) {
                        //            img = img.substr((i + 1) * blockSize);
                        //            console.log('img: ' + (img.length) + ' - ' + img);
                        //            $localstorage.set(imgId + '_' + i, block);
                        //            ++i;
                        //        }
                        //        else {
                        //            console.log('image block break: ' + block);
                        //            break;
                        //        }
                        //    } while (true);
                        //    //$localstorage.set('photo_'+subscribeId+'_'+post.id, dataImg);
                        //    --counter;
                        //    if (counter <= 0) {
                        //        deferred.resolve(result);
                        //    }
                        //});
                    }
                }
                else {
                    result.media.push({
                        type: 'other',
                        data: ''
                    });
                    --counter;
                    if (counter <= 0) {
                        deferred.resolve(result);
                    }
                }
            }
        }
        else {
            deferred.resolve(result);
        }
        return deferred.promise;
    };

    var service = {
        get: function () {
            return posts;
        },
        getCount: function (subscribeId) {
            var count = 0;
            var p = $localstorage.getObject('posts_' + subscribeId, 'null');
            if (p !== null) {
                for (var ind in p) {
                    if (p.hasOwnProperty(ind)) count++;
                }
            }
            return count;
        },
        load: function (subscribeId, callback) {
            loadPosts(subscribeId).then(function (posts) {
                console.log("Posts load data: " + JSON.stringify(posts));
                if (callback) {
                    callback(posts);
                }
            }, function (posts) {
                console.log("Posts load ERROR data: " + JSON.stringify(posts));
                if (callback) {
                    callback(posts);
                }
            });
        },
        update: function (subscribeId, callback) {
            updatePosts(subscribeId).then(function (posts) {
                console.log("Posts update data: " + JSON.stringify(posts));
                if (callback) {
                    callback(posts);
                }
            }, function (posts) {
                console.log("Posts update ERROR data: " + JSON.stringify(posts));
                if (callback) {
                    callback(posts);
                }
            });
        }
    };

    return service;
});