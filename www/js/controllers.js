var app = angular.module('starter.controllers', []);

app.controller('SearchSubscribeCtrl', function ($scope, $state, Subscribe) {
    $scope.searchObj = {subscribeName: '', subscribeId: ''};
    $scope.result = [];

    $scope.$on('$ionicView.enter', function () {
        $scope.searchObj = {subscribeName: '', subscribeId: ''};
        $scope.result = [];
    });

    $scope.searchSubscribe = function () {
        $scope.result = [];
        Subscribe.searchSubscribeById($scope.searchObj.subscribeId).then(function (data) {
            $scope.result = data;
        });
    };

    $scope.addSubscribe = function (subscribe) {
        Subscribe.addSubscribe(subscribe);
        $state.go('tab.list');
    };

    $scope.backInvoke = function () {
        $state.go('tab.list');
    };
});

app.controller('ListCtrl', function ($scope, $state, Subscribe, Posts) {
    $scope.list = Subscribe.getSubscribes();

    $scope.$on('$ionicView.enter', function () {
        $scope.list = Subscribe.getSubscribes();
    });

    $scope.getPostsCount = function (subscribeId) {
        //Posts.getCount(subscribeId).then(function (count) {
        //    return count;
        //});
        //return 'null';
        return Posts.getCount(subscribeId);
    };

    $scope.updateAll = function () {
        for(var ind in $scope.list){
            Posts.load($scope.list[ind], function (data) {});
        }
    };

    $scope.addSubscribeInvoke = function () {
        $state.go('tab.search-subscribe');
    };

    $scope.openSubscribe = function (subscribeId) {
        $state.go('tab.subscribe', {subscribeId: subscribeId});
    };
});

app.controller('SubscribeCtrl', function ($scope, $state, $stateParams, $localstorage, Subscribe, Posts) {
    $scope.subscribe = Subscribe.getSubscribe($stateParams.subscribeId);
    $scope.posts = [];
    $scope.postsCount = 0;

    var reversePosts = function(input){
        var arr = [];
        for(var i in input){
            arr.push(input[i]);
        }
        return arr.reverse();
    };

    $scope.$on('$ionicView.enter', function () {
        $scope.subscribe = Subscribe.getSubscribe($stateParams.subscribeId);
        console.log("SUBSCRIBE #" + $stateParams.subscribeId + ":  " + $scope.subscribe.name);
        Posts.load($stateParams.subscribeId, function (data) {
            console.log("SUBSCRIBE data: " + JSON.stringify(data));
            $scope.posts = reversePosts(data);
            $scope.postsCount = Posts.getCount($stateParams.subscribeId);
        });
        //console.log("SUBSCRIBE list #" + $stateParams.subscribeId + ":  " + JSON.stringify($scope.posts[0]));
    });

    //$scope.convertDate = function (date) {
    //    var d = new Date(''+date+'000');
    //    return "" + d.getHours() + ":" + d.getMinutes();
    //};

    $scope.getPhoto = function(post, url){
        return $localstorage.get('photo_'+$stateParams.subscribeId+'_'+post.id, '');
    };

    $scope.isFavorite = function (postId) {
        return false;//((postId % 2) === 0);
    };

    $scope.refreshInvoke = function () {
        Posts.update($stateParams.subscribeId, function (data) {
            $scope.posts = reversePosts(data);
            $scope.postsCount = Posts.getCount($stateParams.subscribeId);
        });
    };

    $scope.openPost = function (postId) {
        $state.go('tab.post', {subscribeId: $stateParams.subscribeId, postId: postId});
    };

    $scope.backInvoke = function () {
        $state.go('tab.list');
    };
});

app.controller('PostCtrl', function ($scope, $state, $stateParams, $localstorage, Subscribe, Posts) {
    $scope.subscribe = Subscribe.getSubscribe($stateParams.subscribeId);
    $scope.post = null;

    $scope.$on('$ionicView.enter', function () {
        $scope.subscribe = Subscribe.getSubscribe($stateParams.subscribeId);
        Posts.load($stateParams.subscribeId, function(posts){
            $scope.post = posts[$stateParams.postId];
        });
    });

    $scope.getPhoto = function(post){
        return $localstorage.get('photo_'+$stateParams.subscribeId+'_'+post.id, '');
    };

    //$scope.convertDate = function (date) {
    //    var d = new Date(''+date+'000');
    //    return "" + d.getHours() + ":" + d.getMinutes();
    //};

    $scope.isFavorite = function (postId) {
        return ((postId % 2) === 0);
    };

    $scope.next = function () {
        Posts.load($stateParams.subscribeId, function(posts){
            var result = null;
            for(var ind in posts){
                if(result === null){
                    var post = posts[ind];
                    if(post.id > $scope.post.id){
                        result = post;
                    }
                }
                else{
                    break;
                }
            }
            $scope.post = result;
        });

        //var pid = Posts.getPreviousPost($stateParams.subscribeId, $stateParams.postId).id;
        //console.log("previous post id: "+pid);
        //$state.go('tab.post', {subscribeId: $stateParams.subscribeId,
        //    postId: pid});
    };

    $scope.previous = function () {
        Posts.load($stateParams.subscribeId, function(posts){
            var result = null;
            for(var ind in posts){
                var post = posts[ind];
                if(result !== null){
                    if(post.id == $scope.post.id){
                        break;
                    }
                }
                result = post;
            }
            $scope.post = result;
        });
        //$state.go('tab.post', {subscribeId: $stateParams.subscribeId,
        //    postId: Posts.getNextPost($stateParams.subscribeId, $stateParams.postId).id});
    };

    $scope.backInvoke = function () {
        $state.go('tab.subscribe', {subscribeId: $stateParams.subscribeId});
    };
});